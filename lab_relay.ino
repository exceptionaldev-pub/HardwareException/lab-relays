// This example uses an Arduino Uno together with
// an Ethernet Shield to connect to shiftr.io.
//
// You can check on your device after a successful
// connection here: https://shiftr.io/try.
//
// by Joël Gähwiler
// https://github.com/256dpi/arduino-mqtt

#include <Ethernet.h>
#include <MQTT.h>

byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};
byte ip[] = {192, 168, 1, 177};  // <- change to match your network

EthernetClient net; 
MQTTClient client;

unsigned long lastMillis = 0;
int pinOCD = 2;
int pinL1 = 3;
int pinL2 = 4;
int pinL3 = 5;
int pinL4 = 6;
int pinS5 = 7;
int pinS4 = 8;
int pinS3 = 9;

const String opencloseDoor =  "Labs/AI/door/openclose";
const String onoffLight1 =  "Labs/AI/Light/1";
const String onoffLight2 =  "Labs/AI/Light/2";
const String onoffLight3 =  "Labs/AI/Light/3";
const String onoffLight4 =  "Labs/AI/Light/4";
const String onoffSwitch5 =  "Labs/AI/Switch/5";
const String onoffSwitch4 =  "Labs/AI/Switch/4";
const String onoffSwitch3 =  "Labs/AI/Switch/3";

void connect() {
  Serial.print("connecting...");
  while (!client.connect("Relay1", "username", "password")) {
    Serial.print(".");
    delay(1000);
  }

  Serial.println("\nconnected!");

  client.subscribe(opencloseDoor, 2);
  client.subscribe(onoffLight1, 2);
  client.subscribe(onoffLight2, 2);
  client.subscribe(onoffLight3, 2);
  client.subscribe(onoffLight4, 2);
  client.subscribe(onoffSwitch5, 2);
  client.subscribe(onoffSwitch4, 2);
  client.subscribe(onoffSwitch3, 2);
}

void messageReceived(String &topic, String &payload) {
  Serial.println("incoming: " + topic + " - " + payload);
  if (topic == opencloseDoor) {
    if (payload == "open")  {
      digitalWrite(pinOCD, LOW);
      delay(1000);
      digitalWrite(pinOCD, HIGH);
      client.publish("Labs/AI/door/openclose/status", "Signal Receive");
    }
  } else {
    onoffLight(topic, payload);
  }

}

void onoffLight(String topic, String payload) {

  if (topic == onoffLight1) {
    onoffLightWorker(pinL1, payload, "Light/1");
  }
  else if (topic == onoffLight2) {
    onoffLightWorker(pinL2, payload, "Light/2");
  }
  else if (topic == onoffLight3) {
    onoffLightWorker(pinL3, payload, "Light/3");
  }
  else if (topic == onoffLight4) {
    onoffLightWorker(pinL4, payload, "Light/4");
  }
  else if (topic == onoffSwitch5) {
    onoffLightWorker(pinS5, payload, "Switch/5");
  }
  else if (topic == onoffSwitch4) {
    onoffLightWorker(pinS4, payload, "Switch/4");
  }
  else if (topic == onoffSwitch3) {
    onoffLightWorker(pinS3, payload, "Switch/3");
  }
}

void onoffLightWorker(int pin, String payload, String str) {
  if (payload == "on")  {
    digitalWrite(pin, HIGH);
    client.publish("Labs/AI/" + str + "/status", "on");
  } else if (payload == "off")  {
    digitalWrite(pin, LOW);
    client.publish("Labs/AI/" + str + "/status", "off");
  }
}

void setup() {
  Serial.begin(115200);
  Ethernet.begin(mac, ip);

  pinMode(pinOCD, OUTPUT);
  pinMode(pinL1, OUTPUT);
  pinMode(pinL2, OUTPUT);
  pinMode(pinL3, OUTPUT);
  pinMode(pinL4, OUTPUT);
  pinMode(pinS5, OUTPUT);
  pinMode(pinS4, OUTPUT);
  pinMode(pinS3, OUTPUT);


  digitalWrite(pinOCD, HIGH);
  digitalWrite(pinL1, HIGH);
  digitalWrite(pinL2, HIGH);
  digitalWrite(pinL3, HIGH);
  digitalWrite(pinL4, HIGH);
  digitalWrite(pinS5, HIGH);
  digitalWrite(pinS4, HIGH);
  digitalWrite(pinS3, HIGH);

  client.begin("192.168.1.7", 8883, net);
  client.onMessage(messageReceived);

  connect();
}

void loop() {
  client.loop();

  if (!client.connected()) {
    connect();
  }
}